package mx.com.praxis.android.location.sample

import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import mx.com.praxis.android.location.R
import mx.com.praxis.android.locationlibrary.UserLocation
import mx.com.praxis.android.locationlibrary.network.NetworkHelper

class MainActivity : AppCompatActivity() {

    companion object {
        val TAG = MainActivity::class.java.simpleName
    }

    var userLocation: UserLocation? = null
    private var textLoc = ""
    private var isFinish = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tvUserLocation.setText(textLoc)

        btnGetLocation.setOnClickListener {
            if (isFinish) {
                isFinish = false
                userLocation?.stopGetLocation()
                tvUserLocation.text = ""
                textLoc = ""
                userLocation = null
                btnGetLocation.text = getString(R.string.btn_get_location)
            } else {
                progressBar.visibility = View.VISIBLE
                getUserLocationFromLib()
            }
        }

        btnReload.setOnClickListener {
            loadNetworkData()
        }

        loadNetworkData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_refresh) {
            loadNetworkData()
            Toast.makeText(this, "Datos actualizados", Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    fun getUserLocationFromLib() {
        userLocation = UserLocation(this@MainActivity, object : UserLocation.UserLocationCallBack {
            override fun permissionDenied() {

                Log.i(TAG, "permission  denied")
                tvUserLocation.text = "Permiso denegado"
                progressBar.visibility = View.GONE
            }

            override fun locationSettingFailed() {

                Log.i(TAG, "setting failed")
                tvUserLocation.text =
                    "Setting falló: Habilita tu GPS manualmente e intenta de nuevo"
                progressBar.visibility = View.GONE
            }

            override fun getLocation(location: Location) {

                Log.i(TAG, " latitude ${location?.latitude} longitude ${location?.longitude}")

                textLoc += "\nlatitud: ${location?.latitude}, longitud: ${location?.longitude}"
                tvUserLocation.text = textLoc
                progressBar.visibility = View.GONE

                //Detener
                btnGetLocation.text = getString(R.string.btn_stop_location)
                isFinish = true
            }
        })
    }


    fun loadNetworkData() {
        //Datos de red
        if (NetworkHelper.isNetworkEnabled(this)) {
            tvIpAddress.text = NetworkHelper.getIpAddressNetwork(this)
            tvIsVpn.text = if (NetworkHelper.isVpnNetwork(this)) "true" else "false"
        }
    }
}