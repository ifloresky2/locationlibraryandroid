# LocationLibrary

Librería para obtener la ubicación del usuario, importando el archivo .aar

Paso 1. Copia el archivo "locationlibrary.aar" en la carpeta "Projectname/app/libs" de tu proyecto.

Paso 2. En Android Studio, abre el archivo build.gradle de la app en el editor y antes de las dependencias agrega lo siguiente:

    repositories {
        flatDir {
            dirs 'libs'
        }
    }

Paso 3. Añade la siguiente línea en la sección de dependencias:
    
    //AAR LIBS
    implementation (name: 'locationlibrary', ext:'aar')


# Ejemplo de uso:

En la clase donde requieres obtener la ubicación del usuario, utiliza el siguiente código:
     
     
     UserLocation(this@MainActivity, object: UserLocation.UserLocationCallBack{
            override fun permissionDenied() {

                Log.i(TAG, "permiso  denegago")
                //Do Something
            }

            override fun locationSettingFailed() {

                Log.i(TAG, "setting failed")
                //Do Something
            }

            override fun getLocation(location: Location) {

                Log.i(TAG," latitude ${location?.latitude} longitude ${location?.longitude}")
                //Do Something
            }
        })


# Obtener Dirección IP de RED y VPN

        if (NetworkHelper.isNetworkEnabled(this)) {
            tvIpAddress.text = NetworkHelper.getIpAddressNetwork(this)
            tvIsVpn.text = if (NetworkHelper.isVpnNetwork(this)) "true" else "false"
        }