package mx.com.praxis.android.locationlibrary.network

import android.app.Activity
import android.content.Context
import android.content.Context.WIFI_SERVICE
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.wifi.WifiManager
import android.os.Build
import android.text.format.Formatter.formatIpAddress
import androidx.annotation.RequiresApi
import java.net.InetAddress
import java.net.NetworkInterface
import java.util.*


object NetworkHelper {

    fun getIpAddressNetwork(mActivity: Activity): String? {
        if (NetworkHelper.isWifiAvailable(mActivity))
            return NetworkHelper.getWifiIPAddress(mActivity)
        else
            return NetworkHelper.getMobileIPAddress()
    }

    /***
     * This method indicate if wifi is available
     *
     * @param mContext is the context of the app
     * @return boolean if exist wifi connection
     */
    private fun isWifiAvailable(mContext: Context?): Boolean {
        if (mContext != null) {
            val connectivityManager = mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo

            if (networkInfo != null && networkInfo.type == ConnectivityManager.TYPE_WIFI) {
                return true
            }
        }
        return false
    }

    /***
     * This method indicate if wifi or 3G is available
     *
     * @param mContext is the context of the app
     * @return boolean if exist wifi or 3G connection
     */
    fun isNetworkEnabled(mContext: Context?): Boolean {
        if (mContext != null) {
            val connectivityManager = mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = connectivityManager.activeNetworkInfo

            if (networkInfo != null && (networkInfo.type == ConnectivityManager.TYPE_WIFI || networkInfo.type == ConnectivityManager.TYPE_MOBILE)) {
                return true
            }
        }
        return false
    }

    private fun getMobileIPAddress(): String? {
        try {
            val interfaces: List<NetworkInterface> =
                Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs: List<InetAddress> = Collections.list(intf.getInetAddresses())
                for (addr in addrs) {
                    if (!addr.isLoopbackAddress()) {
                        return addr.getHostAddress()
                    }
                }
            }
        } catch (ex: Exception) {
        } // for now eat exceptions
        return ""
    }


    private fun getWifiIPAddress(mActivity: Activity): String? {
        val wifiMgr = mActivity.getApplicationContext().getSystemService(WIFI_SERVICE) as WifiManager?
        val wifiInfo = wifiMgr!!.connectionInfo
        val ip = wifiInfo.ipAddress
        return android.text.format.Formatter.formatIpAddress(ip)
    }

    fun isVpnNetwork(mContext: Context?): Boolean {
        if (mContext != null) {
            val connectivityManager = mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: Network? = connectivityManager.getActiveNetwork()
            val caps: NetworkCapabilities? =
                connectivityManager.getNetworkCapabilities(activeNetwork)
            val vpnInUse = caps?.hasTransport(NetworkCapabilities.TRANSPORT_VPN)

            if (vpnInUse != null)
                return vpnInUse
        }
        return false
    }
}